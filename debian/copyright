Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bulk-extractor
Source: https://github.com/simsong/bulk_extractor
Comment: src/scan_json.cpp contains Evil / Good mentions in license

Files: *
Copyright: 2020 Simson L. Garfinkel
License: Expat
Comment:
 **bulk_extractor** was originally developed by Simson Garfinkel while at
 the Naval Postgraduate School. As a work of the US Government that
 work is not subject to copyright law.
 .
 Simson Garfinkel left the Naval Postgraduate School in January 2015
 and continued to work on **bulk_extractor** in his personal
 capacity. Those modifications are covered under the MIT license (below).
 Other components are licensed as noted.

Files: src/be20_api/dfxml_cpp/src/m4/ax_cxx_compile_stdcxx.m4
       src/be20_api/m4/ax_cxx_compile_stdcxx.m4
       m4/ax_cxx_compile_stdcxx.m4
Copyright: 2008 Benjamin Kosnik <bkoz@redhat.com>
           2012 Zack Weinberg <zackw@panix.com>
           2013 Roy Stogner <roystgnr@ices.utexas.edu>
           2014, 2015 Google Inc.; contributed by Alexey Sokolov <sokolov@google.com>
           2015 Paul Norman <penorman@mac.com>
           2015 Moritz Klammler <moritz@klammler.eu>
           2016, 2018 Krzesimir Nowak <qdlacz@gmail.com>
           2019 Enji Cooper <yaneurabeya@gmail.com>
           2020 Jason Merrill <jason@redhat.com>
           2021 Jörn Heusipp <osmanx@problemloesungsmaschine.de>
License: Other
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.

Files: src/old_scanners/scan_ascii85.cpp
Copyright: 2011 Remy Oukaour
License: Expat

Files: src/base64_forensic.cpp
Copyright: 1996-1999 by Internet Software Consortium.
License: Other
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND INTERNET SOFTWARE CONSORTIUM DISCLAIMS
 ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL INTERNET SOFTWARE
 CONSORTIUM BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 SOFTWARE.
 .
 Portions Copyright (c) 1995 by International Business Machines, Inc.
 .
 International Business Machines, Inc. (hereinafter called IBM) grants
 permission under its copyrights to use, copy, modify, and distribute this
 Software with or without fee, provided that the above copyright notice and
 all paragraphs of this notice appear in all copies, and that the name of IBM
 not be used in connection with the marketing of any product incorporating
 the Software or modifications thereof, without specific, written prior
 permission.
 .
 To the extent it has a right to do so, IBM grants an immunity from suit
 under its patents, if any, for the use, sale or manufacture of products to
 the extent that such products are used for performing Domain Name System
 dynamic updates in TCP/IP networks by means of the Software.  No immunity is
 granted for any product per se or for any other function of any product.
 .
 THE SOFTWARE IS PROVIDED "AS IS", AND IBM DISCLAIMS ALL WARRANTIES,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE.  IN NO EVENT SHALL IBM BE LIABLE FOR ANY SPECIAL,
 DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER ARISING
 OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE, EVEN
 IF IBM IS APPRISED OF THE POSSIBILITY OF SUCH DAMAGES

Files: src/tsk3/*
Copyright: 2007-2010 Brian Carrier
License: CPL-1.0
 THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THIS COMMON PUBLIC
 LICENSE ("AGREEMENT"). ANY USE, REPRODUCTION OR DISTRIBUTION OF THE
 PROGRAM CONSTITUTES RECIPIENT'S ACCEPTANCE OF THIS AGREEMENT.
 .
 1. DEFINITIONS
 .
 "Contribution" means:
 .
    a) in the case of the initial Contributor, the initial code and
    documentation distributed under this Agreement, and
    b) in the case of each subsequent Contributor:
    i) changes to the Program, and
    ii) additions to the Program;
    where such changes and/or additions to the Program originate from and
    are distributed by that particular Contributor. A Contribution
    'originates' from a Contributor if it was added to the Program by such
    Contributor itself or anyone acting on such Contributor's behalf.
    Contributions do not include additions to the Program which: (i) are
    separate modules of software distributed in conjunction with the
    Program under their own license agreement, and (ii) are not derivative
    works of the Program.
 .
 "Contributor" means any person or entity that distributes the Program.
 .
 "Licensed Patents " mean patent claims licensable by a Contributor which
 are necessarily infringed by the use or sale of its Contribution alone or
 when combined with the Program.
 .
 "Program" means the Contributions distributed in accordance with this
 Agreement.
 .
 "Recipient" means anyone who receives the Program under this Agreement,
 including all Contributors.
 .
 2. GRANT OF RIGHTS
 .
    a) Subject to the terms of this Agreement, each Contributor hereby
    grants Recipient a non-exclusive, worldwide, royalty-free copyright
    license to reproduce, prepare derivative works of, publicly display,
    publicly perform, distribute and sublicense the Contribution of such
    Contributor, if any, and such derivative works, in source code and
    object code form.
 .
    b) Subject to the terms of this Agreement, each Contributor hereby
    grants Recipient a non-exclusive, worldwide, royalty-free patent
    license under Licensed Patents to make, use, sell, offer to sell,
    import and otherwise transfer the Contribution of such Contributor, if
    any, in source code and object code form. This patent license shall
    apply to the combination of the Contribution and the Program if, at
    the time the Contribution is added by the Contributor, such addition
    of the Contribution causes such combination to be covered by the
    Licensed Patents. The patent license shall not apply to any other
    combinations which include the Contribution. No hardware per se is
    licensed hereunder.
 .
    c) Recipient understands that although each Contributor grants the
    licenses to its Contributions set forth herein, no assurances are
    provided by any Contributor that the Program does not infringe the
    patent or other intellectual property rights of any other entity. Each
    Contributor disclaims any liability to Recipient for claims brought by
    any other entity based on infringement of intellectual property rights
    or otherwise. As a condition to exercising the rights and licenses
    granted hereunder, each Recipient hereby assumes sole responsibility
    to secure any other intellectual property rights needed, if any. For
    example, if a third party patent license is required to allow
    Recipient to distribute the Program, it is Recipient's responsibility
    to acquire that license before distributing the Program.
 .
    d) Each Contributor represents that to its knowledge it has sufficient
    copyright rights in its Contribution, if any, to grant the copyright
    license set forth in this Agreement.
 .
 3. REQUIREMENTS
 .
 A Contributor may choose to distribute the Program in object code form
 under its own license agreement, provided that:
 .
    a) it complies with the terms and conditions of this Agreement; and
    b) its license agreement:
    i) effectively disclaims on behalf of all Contributors all warranties
    and conditions, express and implied, including warranties or
    conditions of title and non-infringement, and implied warranties or
    conditions of merchantability and fitness for a particular purpose;
    ii) effectively excludes on behalf of all Contributors all liability
    for damages, including direct, indirect, special, incidental and
    consequential damages, such as lost profits;
    iii) states that any provisions which differ from this Agreement are
    offered by that Contributor alone and not by any other party; and
    iv) states that source code for the Program is available from such
    Contributor, and informs licensees how to obtain it in a reasonable
    manner on or through a medium customarily used for software exchange.
 .
 When the Program is made available in source code form:
    a) it must be made available under this Agreement; and
    b) a copy of this Agreement must be included with each copy of the Program.
 .
 Contributors may not remove or alter any copyright notices contained
 within the Program.
 .
 Each Contributor must identify itself as the originator of its
 Contribution, if any, in a manner that reasonably allows subsequent
 Recipients to identify the originator of the Contribution.
 .
 4. COMMERCIAL DISTRIBUTION
 .
 Commercial distributors of software may accept certain responsibilities
 with respect to end users, business partners and the like. While this
 license is intended to facilitate the commercial use of the Program, the
 Contributor who includes the Program in a commercial product offering
 should do so in a manner which does not create potential liability for
 other Contributors. Therefore, if a Contributor includes the Program in a
 commercial product offering, such Contributor ("Commercial Contributor")
 hereby agrees to defend and indemnify every other Contributor
 ("Indemnified Contributor") against any losses, damages and costs
 (collectively "Losses") arising from claims, lawsuits and other legal
 actions brought by a third party against the Indemnified Contributor to
 the extent caused by the acts or omissions of such Commercial Contributor
 in connection with its distribution of the Program in a commercial
 product offering. The obligations in this section do not apply to any
 claims or Losses relating to any actual or alleged intellectual property
 infringement. In order to qualify, an Indemnified Contributor must: a)
 promptly notify the Commercial Contributor in writing of such claim, and
 b) allow the Commercial Contributor to control, and cooperate with the
 Commercial Contributor in, the defense and any related settlement
 negotiations. The Indemnified Contributor may participate in any such
 claim at its own expense.
 .
 For example, a Contributor might include the Program in a commercial
 product offering, Product X. That Contributor is then a Commercial
 Contributor. If that Commercial Contributor then makes performance
 claims, or offers warranties related to Product X, those performance
 claims and warranties are such Commercial Contributor's responsibility
 alone. Under this section, the Commercial Contributor would have to
 defend claims against the other Contributors related to those performance
 claims and warranties, and if a court requires any other Contributor to
 pay any damages as a result, the Commercial Contributor must pay those
 damages.
 .
 5. NO WARRANTY
 .
 EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, THE PROGRAM IS PROVIDED
 ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER
 EXPRESS OR IMPLIED INCLUDING, WITHOUT LIMITATION, ANY WARRANTIES OR
 CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A
 PARTICULAR PURPOSE. Each Recipient is solely responsible for determining
 the appropriateness of using and distributing the Program and assumes all
 risks associated with its exercise of rights under this Agreement,
 including but not limited to the risks and costs of program errors,
 compliance with applicable laws, damage to or loss of data, programs or
 equipment, and unavailability or interruption of operations.
 .
 6. DISCLAIMER OF LIABILITY
 .
 EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, NEITHER RECIPIENT NOR
 ANY CONTRIBUTORS SHALL HAVE ANY LIABILITY FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING
 WITHOUT LIMITATION LOST PROFITS), HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OR
 DISTRIBUTION OF THE PROGRAM OR THE EXERCISE OF ANY RIGHTS GRANTED
 HEREUNDER, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 .
 7. GENERAL
 .
 If any provision of this Agreement is invalid or unenforceable under
 applicable law, it shall not affect the validity or enforceability of the
 remainder of the terms of this Agreement, and without further action by
 the parties hereto, such provision shall be reformed to the minimum
 extent necessary to make such provision valid and enforceable.
 .
 If Recipient institutes patent litigation against a Contributor with
 respect to a patent applicable to software (including a cross-claim or
 counterclaim in a lawsuit), then any patent licenses granted by that
 Contributor to such Recipient under this Agreement shall terminate as of
 the date such litigation is filed. In addition, if Recipient institutes
 patent litigation against any entity (including a cross-claim or
 counterclaim in a lawsuit) alleging that the Program itself (excluding
 combinations of the Program with other software or hardware) infringes
 such Recipient's patent(s), then such Recipient's rights granted under
 Section 2(b) shall terminate as of the date such litigation is filed.
 .
 All Recipient's rights under this Agreement shall terminate if it fails
 to comply with any of the material terms or conditions of this Agreement
 and does not cure such failure in a reasonable period of time after
 becoming aware of such noncompliance. If all Recipient's rights under
 this Agreement terminate, Recipient agrees to cease use and distribution
 of the Program as soon as reasonably practicable. However, Recipient's
 obligations under this Agreement and any licenses granted by Recipient
 relating to the Program shall continue and survive.
 .
 Everyone is permitted to copy and distribute copies of this Agreement,
 but in order to avoid inconsistency the Agreement is copyrighted and may
 only be modified in the following manner. The Agreement Steward reserves
 the right to publish new versions (including revisions) of this Agreement
 from time to time. No one other than the Agreement Steward has the right
 to modify this Agreement. IBM is the initial Agreement Steward. IBM may
 assign the responsibility to serve as the Agreement Steward to a suitable
 separate entity. Each new version of the Agreement will be given a
 distinguishing version number. The Program (including Contributions) may
 always be distributed subject to the version of the Agreement under which
 it was received. In addition, after a new version of the Agreement is
 published, Contributor may elect to distribute the Program (including its
 Contributions) under the new version. Except as expressly stated in
 Sections 2(a) and 2(b) above, Recipient receives no rights or licenses to
 the intellectual property of any Contributor under this Agreement,
 whether expressly, by implication, estoppel or otherwise. All rights in
 the Program not expressly granted under this Agreement are reserved.
 .
 This Agreement is governed by the laws of the State of New York and the
 intellectual property laws of the United States of America. No party to
 this Agreement will bring a legal action under this Agreement more than
 one year after the cause of action arose. Each party waives its rights to
 a jury trial in any resulting litigation.

Files: src/scan_json.cpp
Copyright: 2005 JSON.org
           2021 Simson L. Garfinkel and Jan Gruber
License: Other-evil
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 The Software shall be used for Good, not Evil.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: src/pyxpress.h
Copyright: 2008 (c) Matthieu Suiche. <msuiche[at]gmail.com>
License: GPL-3+ with OpenSSL exception
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 In addition, as a special exception, the copyright holders give
 permission to link the code of portions of this program with the
 OpenSSL library under certain conditions as described in each
 individual source file, and distribute linked combinations
 including the two.
 .
 You must obey the GNU General Public License in all respects
 for all of the code used other than OpenSSL.  If you modify
 file(s) with this exception, you may extend this exception to your
 version of the file(s), but you are not obligated to do so.  If you
 do not wish to do so, delete this exception statement from your
 version.  If you delete this exception statement from all source
 files in the program, then also delete it here.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

Files: src/scan_elf.cpp
Copyright: Unknown
License: LGPL-2.1

Files: src/scan_outlook.h
Copyright: 2012 (C) Simson L. Garfinkel
           2008-2014, Joachim Metz <joachim.metz@gmail.com>
License: LGPL-3+

Files: m4/ac_prog_java.m4 m4/ac_prog_java_works.m4 m4/ac_check_rqrd_class.m4
 m4/ax_pthread.m4 m4/ac_prog_java_cc.m4 m4/ac_prog_javac.m4
 m4/ac_prog_javac_works.m4 m4/ac_check_classpath.m4
Copyright: 2000 Stephane Bortzmeyer <bortzmeyer@pasteur.fr>
           2002 Nic Ferrier <nferrier@tapsellferrier.co.uk>
           2008 Steven G. Johnson <stevenj@alum.mit.edu>
           2011 Daniel Richard G. <skunk@iSKUNK.ORG>
License: GPL-3 with Autoconf exception

Files: src/be20_api/utfcpp/source/utf8/cpp20.h
Copyright: 2022 Nemanja Trifunovic
License: Boost-1.0
 Permission is hereby granted, free of charge, to any person or
 organization obtaining a copy of the software and accompanying
 documentation covered by this license (the "Software") to use, reproduce,
 display, distribute, execute, and transmit the Software, and to prepare
 derivative works of the Software, and to permit third-parties to whom the
 Software is furnished to do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement,
 including the above license grant, this restriction and the following
 disclaimer, must be included in all copies of the Software, in whole or
 in part, and all derivative works of the Software, unless such copies or
 derivative works are solely in the form of machine-executable object code
 generated by a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND
 NON-INFRINGEMENT. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR ANYONE
 DISTRIBUTING THE SOFTWARE BE LIABLE FOR ANY DAMAGES OR OTHER LIABILITY,
 WHETHER IN CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: debian/*
Copyright: 2012 dookie <dookie@kali.org>
           2024 Sophie Brun <sophie@offensive-security.com>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: LGPL-2.1
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or (at
 your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this library; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-3+
 On Debian systems the full text of the LGPL-3 can be found in
 /usr/share/common-licenses/LGPL-3

License: GPL-3 with Autoconf exception
 As a special exception to the GNU General Public License,
 if you distribute this file as part of a program
 that contains a configuration script generated by Autoconf,
 you may include it under the same distribution terms
 that you use for the rest of that program.
 This Exception is an additional permission
 under section 7 of the GNU General Public License, version 3 ("GPLv3").
