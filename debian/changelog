bulk-extractor (2.1.1-0kali2) kali-dev; urgency=medium

  * Update build-dep (new libboost)
  * Fix the build for the new version libre2-11

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 12 Jun 2024 15:11:12 +0200

bulk-extractor (2.1.1-0kali1) kali-dev; urgency=medium

  * New upstream version 2.1.1
  * Update Uploaders
  * Update debian/copyright

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 30 Apr 2024 11:37:01 +0200

bulk-extractor (2.1.0-0kali1) kali-dev; urgency=medium

  * New upstream version 2.1.0
  * Add python3 and libre2-dev as build-dep + update d/rules

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 29 Jan 2024 17:19:06 +0100

bulk-extractor (2.0.6-0kali1) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * New upstream version 2.0.3

  [ Sophie Brun ]
  * New upstream version 2.0.6
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 10 Jan 2024 15:32:39 +0100

bulk-extractor (2.0.3-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.3
  * Fix building with gcc-13

 -- Steev Klimaszewski <steev@kali.org>  Tue, 03 Oct 2023 15:33:48 -0500

bulk-extractor (2.0.0-0kali2) kali-dev; urgency=medium

  [ Sophie Brun ]
  * Disable i386 build on CI

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.
  * Add ITP bugs in 1.3-0kali1.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Set upstream metadata fields: Repository.
  * Update standards version to 4.6.2, no changes needed.

  [ Steev Klimaszewski ]
  * Add lintian overrides.
  * ci: Disable 32bit arm architectures.
  * Prepare for Release

 -- Steev Klimaszewski <steev@kali.org>  Mon, 17 Apr 2023 10:50:59 -0500

bulk-extractor (2.0.0-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.0
  * Refresh debian/patches
  * Update debian/copyright
  * Restrict to 64 bits arch
    (https://github.com/simsong/bulk_extractor/issues/318)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 15 Feb 2022 12:11:50 +0100

bulk-extractor (2.0.0~beta3-0kali1) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Update email address
  * Update email address
  * Add Uploaders
  * Remove template comment and switch spaces to tabs

  [ Sophie Brun ]
  * New upstream version 2.0.0~beta3

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 15 Dec 2021 10:27:20 +0100

bulk-extractor (2.0.0~beta2-0kali3) kali-dev; urgency=medium

  * Add -latomic in LIBS not in LDFLAGS for armel build

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 02 Nov 2021 18:07:47 +0100

bulk-extractor (2.0.0~beta2-0kali2) kali-dev; urgency=medium

  * Add -latomic only for specific arch

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 28 Oct 2021 14:16:18 +0200

bulk-extractor (2.0.0~beta2-0kali1) kali-dev; urgency=medium

  * New upstream version 2.0.0~beta2
  * Update debian/copyright
  * Add missing LDFLAGS -latomic for armel

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 27 Oct 2021 09:53:33 +0200

bulk-extractor (2.0.0~beta1-0kali1) kali-dev; urgency=medium

  * Update debian/watch
  * New upstream version 2.0.0~beta1
  * Update debian/rules
  * Refresh patches
  * Update debian/copyright
  * Build-dep: add missing libxml2-utils
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 19 Oct 2021 16:25:50 +0200

bulk-extractor (1.6.0-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Maintainer field
  * Update Vcs-* fields for the move to gitlab.com
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 1.6.0
  * Bump Standards-Version to 4.4.1
  * Update debian/copyright
  * Use debhelper-compat 12
  * Add hardening flags
  * Add a patch to fix spelling errors

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 12 Nov 2019 11:52:31 +0100

bulk-extractor (1.5.3+git20150907-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 07 Sep 2015 16:06:18 +0200

bulk-extractor (1.5.3-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 07 Sep 2015 15:50:16 +0200

bulk-extractor (1.5.1-0kali2) kali-dev; urgency=medium

  * drop depends to libraries (already managed by shlibs:Depends)

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 06 Aug 2014 08:44:21 +0200

bulk-extractor (1.5.1-0kali1) kali-dev; urgency=medium

  * debian/watch updated because it didn't work.
  * New Uptream release
  * drop 2 files in debian/docs because they don't exist anymore
  * add dh_autoreconf in rules to create ./configure
  * Use debhelper 9 nad change url for vcs-git
  * Rearrange debian/copyright

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 05 Aug 2014 09:48:08 +0200

bulk-extractor (1.3-1kali5) kali; urgency=low

  * Removed desktop file

 -- Mati Aharoni <muts@kali.org>  Fri, 14 Dec 2012 19:49:22 -0500

bulk-extractor (1.3-1kali4) kali; urgency=low

  * Updated desktop file

 -- Mati Aharoni <muts@kali.org>  Sat, 01 Dec 2012 10:50:03 -0500

bulk-extractor (1.3-1kali3) kali; urgency=low

  * Fixed build and installation depends

 -- Devon Kearns <dookie@kali.org>  Thu, 08 Nov 2012 11:50:37 -0700

bulk-extractor (1.3-1kali2) kali; urgency=low

  * Kali Version with fixed build depends

 -- Devon Kearns <dookie@kali.org>  Thu, 08 Nov 2012 11:35:21 -0700

bulk-extractor (1.3-1kali1) kali; urgency=low

  * Kali Version

 -- Devon Kearns <dookie@kali.org>  Tue, 06 Nov 2012 14:36:10 -0700

bulk-extractor (1.3-0kali1) kali; urgency=low

  * Initial release. Closes: #990302

 -- Devon Kearns <dookie@kali.org>  Tue, 06 Nov 2012 14:14:48 -0700
